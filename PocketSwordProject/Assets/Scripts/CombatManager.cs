using System.Collections;
using UnityEngine;

public class CombatManager : MonoBehaviour
{
    public enum Action { Attack, Block, Dodge }

    private Action[] playerActions = new Action[3];
    private Action[] enemyActions = new Action[3];

    private int actionsSelected = 0;

    void Start()
    {
        UIManager.Instance.ClearEnemyTexts();
        GenerateEnemyActions();
    }

    void GenerateEnemyActions()
    {
        for (int i = 0; i < 3; i++)
        {
            enemyActions[i] = (Action)Random.Range(0, 3);
            UIManager.Instance.PutEnemyText(i, enemyActions[i].ToString());
            Debug.Log("Enemy number " + i.ToString() + " chose: " + enemyActions[i].ToString());
        }
    }

    public void SetPlayerAction(int actionIndex)
    {
        if (actionsSelected < 3)
        {
            playerActions[actionsSelected] = (Action)actionIndex;
            actionsSelected++;

            // Si el jugador ha seleccionado las 3 acciones, resolver el combate
            if (actionsSelected == 3)
            {
                ResolveCombat();
            }
        }
    }

    void ResolveCombat()
    {
        for (int i = 0; i < 3; i++)
        {
            // Comparar la acci�n del jugador con la acci�n del enemigo correspondiente
            CompareActions(i);
        }

        // Generar nuevas acciones enemigas para el pr�ximo turno
        GenerateEnemyActions();

        // Reiniciar el contador de acciones seleccionadas
        actionsSelected = 0;
    }

    void CompareActions(int index)
    {
        if (playerActions[index] == enemyActions[index])
        {
            Debug.Log("Fight " + (index + 1) + ": It's a tie!");
            // Aqu� puedes a�adir l�gica para un empate
        }
        else if ((playerActions[index] == Action.Attack && enemyActions[index] == Action.Dodge) ||
                 (playerActions[index] == Action.Block && enemyActions[index] == Action.Attack) ||
                 (playerActions[index] == Action.Dodge && enemyActions[index] == Action.Block))
        {
            Debug.Log("Fight " + (index + 1) + ": Player wins!");
            // Aqu� puedes a�adir l�gica para un minijuego de victoria
        }
        else
        {
            Debug.Log("Fight " + (index + 1) + ": Player loses!");
            // Aqu� puedes a�adir l�gica para un minijuego de derrota o p�rdida de vida
        }
    }
}
