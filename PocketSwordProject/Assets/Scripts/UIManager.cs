using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance
    {
        get { return instance; }
    }

    public CombatManager combatManager;
    public List<TMP_Text> lstEnemyTexts;


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    public void OnAttackButtonPressed()
    {
        combatManager.SetPlayerAction((int)CombatManager.Action.Attack);
    }

    public void OnBlockButtonPressed()
    {
        combatManager.SetPlayerAction((int)CombatManager.Action.Block);
    }

    public void OnDodgeButtonPressed()
    {
        combatManager.SetPlayerAction((int)CombatManager.Action.Dodge);
    }

    public void ClearEnemyTexts() 
    {
        foreach (var txt in lstEnemyTexts)
        {
            txt.text = " ";
        }
    
    }

    public void PutEnemyText(int parNumber, string parTxt) 
    {
        lstEnemyTexts[parNumber].text = parTxt;
    
    }
}
